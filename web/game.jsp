<%@ page import="controller.Game" %>
<%@ page import="controller.Player" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="controller.Card" %>
<%@ page import="utils.CardValue" %>
<%@ page import="utils.CardType" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String userName = (String) session.getAttribute("user");
    if (null == userName) {
        RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
        rd.forward(request, response);
    }
%>
<html>
    <head>
        <title>Board Game</title>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"
                integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
                crossorigin="anonymous"></script>
    </head>
    <body>
        <form action="game" method="post" id="formGame">
            <div align="right">
                <% String username = (String) request.getSession().getAttribute("user"); %>
                Hi again, <% out.println(username); %> !
                <input type="submit" name="logout" value="Logout"/>
            </div>
            <div align="left" id="buttons">
                <% int player = (int) ((Game) request.getSession().getAttribute("game")).getCurrentPlayer().getUser().getIdUser();
                    int idUser = (int) request.getSession().getAttribute("id");
                    if (player == idUser)
                        out.print("<input type=\"submit\" name=\"drawCard\" value=\"Draw Card\"/>" +
                                "<input type=\"submit\" name=\"startGame\" value=\"Start game\"/>");
                    else
                        out.print("<input type=\"submit\" name=\"drawCard\" value=\"Draw Card\" disabled/>" +
                                "<input type=\"submit\" name=\"startGame\" value=\"Start game\" disabled/>");
                %>
            </div>
            <div align="center">
                <h4>Down Card</h4>
                <div id="card">
                    <%
                        Card downCard = (Card) request.getSession().getAttribute("downCard");
                        if (downCard != null && downCard.getCardValue() != CardValue.INVALID_VALUE && downCard.getCardType() != CardType.INVALID_TYPE) {
                            out.print("<td width='70' height='100'><img src=\"./images/" + downCard.getCardValue().toString() + downCard.getCardType().toString().charAt(0) + ".png\" width=\"70\" height=\"100\"></td>");
                        }
                    %>
                </div>
                <br/>
                <br/>
                <table>
                    <tbody>
                        <%
                            ArrayList<Card> cards = (ArrayList<Card>) request.getSession().getAttribute("playerCards");
                            int buttonNo = 0;
                            for (Card card : cards) {
                                out.print("<td><button type=\"submit\" name=\"button" + buttonNo++ + "\" style=\"border:1px; background-color: transparent;\"><img src=\"./images/" + card.getCardValue().toString() + card.getCardType().toString().charAt(0) + ".png\" width=\"70\" height=\"100\"></button></td>");
                            }
                            out.flush();
                        %>
                    </tbody>
                </table>
            </div>
        </form>

        <form action="changeCard" method="post" id="formCardType">
            <%
                if (player != idUser) out.print("<select name=\"cardType\" id=\"cardType\">" +
                        "<option value=\"HEARTS\">HEARTS</option>" +
                        "<option value=\"DIAMONDS\">DIAMONDS</option>" +
                        "<option value=\"CLUBS\">CLUBS</option>" +
                        "<option value=\"SPADES\">SPADES</option>" +
                        "</select>" +
                        "<input type=\"submit\"/>");
            %>
            <div id="type">
                <%=request.getSession().getAttribute("changedCardType")%>
            </div>

        </form>
    </body>
    <script>
        function getCard() {

            $.ajax({
                type: "GET",
                url: "http://localhost:8080/macao_war_exploded/game",
                success: function (data) {
                    var cards = JSON.parse(data);
                    console.log(cards);

                    if (cards[0] != null && cards[0].cardValue != "INVALID_VALUE" && cards[0].cardType != "INVALID_TYPE") {
                        $('#card').html("<td width='70' height='100'><img src=\"./images/" + cards[0].cardValue + cards[0].cardType[0] + ".png\" width=\"70\" height=\"100\"></td>");
                    }

                    if (cards.length > 1) {
                        var buttonNo = 0;
                        $('tbody').html('');
                        for (var i = 1; i < cards.length; i++) {
                            var card = cards[i];
                            $('tbody').html($('tbody').html() +
                                "<td><button type=\"submit\" name=\"button" + buttonNo++ + "\" style=\"border:1px; background-color: transparent;\"><img src=\"./images/" + card.cardValue + card.cardType[0] + ".png\" width=\"70\" height=\"100\"></button></td>"
                            );
                        }
                        ;
                    }
                }
            });
        }

        function getCardType() {
            $.ajax({
                type: "GET",
                url: "http://localhost:8080/macao_war_exploded/changeCard",
                success: function (data) {
                    var type = JSON.parse(data);
                    console.log(type);

                    $('#type').html(type);
                }
            })
        }

        function doPost() {
            document.getElementById("formGame").submit();
            <%--//var player = <%=(int)((Player)request.getSession().getAttribute("currentPlayer")).getUser().getIdUser()%>--%>
            <%--//var id = <%=(int)request.getSession().getAttribute("id")%>--%>
            <%--//console.log(player + " " + id)--%>
            <%--//if(player === id) {--%>
            <%--    //alert("Time is over!");--%>
            <%--//}--%>
        }

        setInterval(getCard, 300);
        setInterval(getCardType, 300);
        setInterval(doPost, 10000);

    </script>
</html>
