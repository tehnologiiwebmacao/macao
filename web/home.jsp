<%@ page import="model.User" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Date" %>
<%@ page import="controller.Game" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String userName = (String) session.getAttribute("user");
    if (null == userName) {
        RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
        rd.forward(request, response);
    }
%>
<html>
<head>
    <title>Home Page</title>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
            crossorigin="anonymous"></script>
</head>
<body>
<form action="home" method="post">
    <div align="right">
        <% String username = (String)request.getSession().getAttribute("user"); %>
        Hi <% out.println(username); %> !
        </br>
        <input type="submit" name="logout" value="Logout"/>
    </div>
    <div align="center">
        <h3>Home Page</h3>
    <table width="59%" border="1">
        <thead>
        <tr>
            <td>Nr.</td>
            <td>Game Name</td>
            <td>Owner</td>
            <td>Status</td>
            <td>Player No.</td>
            <td></td>
        </tr>
        </thead>
        <tbody>

        <%
            ArrayList<Game> games = (ArrayList<Game>)request.getSession().getAttribute("listGames");
            for (Game game : games) {
                out.print(String.format("<tr>" +
                                "<td>%d</td>" +
                                "<td>%s</td>" +
                                "<td>%s</td>" +
                                "<td>%s</td>" +
                                "<td>%d / 6</td>" +
                                "<td><input type=\"submit\" name=\"button" + game.getId() +"\" value=\"Join" + game.getId() +"\"</td>" +
                                "</tr>",
                        game.getId(), game.getName(), game.getOwner().getUser().getUsername(), game.getGameStatus().toString(), game.getJoinedPlayers().size()));
            }
            out.flush();
        %>

        </tbody>
    </table>
        <div class="games">

        </div>
        <input type="text" placeholder="Numele jocului" name="gameName" id="gameName" />
        <input type="submit" name="addGame" value="Add Game"/>
    </div>
</form>
</body>
<script>
    function getGames() {
        $.ajax({
            type: "GET",
            url: "http://localhost:8080/macao_war_exploded/home",
            success: function( data ) {
                var games = JSON.parse(data);

                $('tbody').html("<tr>" + "</tr>");
                for(var i = 0; i < games.length; i++) {
                    var game = games[i];
                        $('tbody').html($('tbody').html() + "<tr>" +
                            "<td>" + game.id + "</td>" +
                            "<td>" + game.name + "</td>" +
                            "<td>" + game.owner.user.username + "</td>" +
                            "<td>" + game.gameStatus + "</td>" +
                            "<td>" + game.joinedPlayers.length + "/ 6" + "</td>" +
                            "<td><input type=\"submit\" name=\"button" + game.id +"\" value=\"Join" + game.id +"\"</td>" +
                            "</tr>");
                };
            }
        });
    }
    function hasGame(games, game) {
        for(var i = 0; i < games.length; i++) {
            if (games[i].id === game.id)
                return true;
        }
        return false;
    }
    setInterval(getGames, 500);
</script>
</html>
