package controller;

import model.User;
import org.junit.jupiter.api.Test;
import utils.CardAction;
import utils.CardType;
import utils.CardValue;
import utils.PlayerStatus;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Player test.
 */
class PlayerTest {

    /**
     * Give cards.
     */
    @Test
    void giveCards() {
        Player newPlayer = new Player(new User(1, "Test", "pass"));
        Deck newDeck = new Deck();
        final int numberOfCardsToDraw = 5;

        newPlayer.giveCards(newDeck.drawCards(numberOfCardsToDraw));
        assert newPlayer.getCards().size() == numberOfCardsToDraw;
    }

    /**
     * Give card.
     */
    @Test
    void giveCard() {
        Player newPlayer = new Player(new User(1, "Test", "pass"));
        Deck newDeck = new Deck();
        Card cardToGive = newDeck.drawCard();

        newPlayer.giveCard(cardToGive);
        assert newPlayer.getCards().size() == 1;
        assert newPlayer.getCards().get(0).equals(cardToGive);
    }

    /**
     * Discard to pile.
     */
    @Test
    void discardToPile() {
        Player newPlayer = new Player(new User(1, "Test", "pass"));
        Card playerCard = new Card(CardValue.THREE, CardType.SPADES, CardAction.NO_ACTION);
        Card downCard =  new Card(CardValue.THREE, CardType.RED, CardAction.NO_ACTION);

        newPlayer.giveCard(playerCard);
        Card returnedCard = newPlayer.discardToPile(playerCard, downCard);
        assert returnedCard.equals(playerCard);

        Card notMatchCard = new Card(CardValue.FOUR, CardType.CLUBS, CardAction.NO_ACTION);

        newPlayer.giveCard(playerCard);
        Card invalidCard = newPlayer.discardToPile(playerCard, notMatchCard);
        assert invalidCard.equals(playerCard);
    }

    /**
     * Discard all.
     */
    @Test
    void discardAll() {
        Player newPlayer = new Player(new User(1, "Test", "pass"));
        newPlayer.discardAll();

        assert newPlayer.getCards().isEmpty();
        assert newPlayer.getHandSize() == 0;
    }

    /**
     * Gets hand size.
     */
    @Test
    void getHandSize() {
        Player newPlayer = new Player(new User(1, "Test", "pass"));

        assert newPlayer.getHandSize() == 0;

        Card playerCard = new Card(CardValue.THREE, CardType.SPADES, CardAction.NO_ACTION);
        newPlayer.giveCard(playerCard);
        assert newPlayer.getHandSize() == 1;
    }

    /**
     * Increment win.
     */
    @Test
    void incrementWin() {
        Player newPlayer = new Player(new User(1, "Test", "pass"));

        assert newPlayer.getNoWins() == 0;

        newPlayer.incrementWin();
        assert newPlayer.getNoWins() == 1;
    }

    /**
     * Gets no wins.
     */
    @Test
    void getNoWins() {
        Player newPlayer = new Player(new User(1, "Test", "pass"));

        assert newPlayer.getNoWins() == 0;
    }

    /**
     * Sets status.
     */
    @Test
    void setStatus() {
        Player newPlayer = new Player(new User(1, "Test", "pass"));

        newPlayer.setStatus(PlayerStatus.PLAYING);
        assert newPlayer.getStatus().equals(PlayerStatus.PLAYING);
    }

    /**
     * Gets status.
     */
    @Test
    void getStatus() {
        Player newPlayer = new Player(new User(1, "Test", "pass"));

        assert newPlayer.getStatus().equals(PlayerStatus.SPECTATING);
    }

    /**
     * Has stop or get.
     */
    @Test
    void hasStopOrGet() {
        Player newPlayer = new Player(new User(1, "Test", "pass"));

        assert !newPlayer.hasStopOrGet();

        Card playerCard = new Card(CardValue.THREE, CardType.SPADES, CardAction.GET_THREE_CARDS);
        newPlayer.giveCard(playerCard);

        assert newPlayer.hasStopOrGet();
    }

    /**
     * Gets cards.
     */
    @Test
    void getCards() {
        Player newPlayer = new Player(new User(1, "Test", "pass"));
        Deck newDeck = new Deck();
        List<Card> cardsToGive = newDeck.drawCards(10);
        newPlayer.giveCards(cardsToGive);

        List<Card> returnedCards = newPlayer.getCards();
        assert returnedCards.size() == cardsToGive.size();
        for (int i = 0; i < cardsToGive.size(); i++) {
            assert returnedCards.get(i).equals(cardsToGive.get(i));
        }
    }
}