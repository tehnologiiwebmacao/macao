package controller;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Deck test.
 */
class DeckTest {

    /**
     * Reset.
     */
    @Test
    void reset() {
        Deck newDeck = new Deck();
        final int deckSize = 54;

        newDeck.drawCards(1);
        assert newDeck.getDeckSize() == deckSize - 1;
        newDeck.reset();
        assert newDeck.getDeckSize() == deckSize;
    }

    /**
     * Draw card.
     */
    @Test
    void drawCard() {
        Deck newDeck = new Deck();
        final int deckSize = 54;

        newDeck.drawCards(1);
        assert newDeck.getDeckSize() == deckSize - 1;
    }

    /**
     * Gets deck size.
     */
    @Test
    void getDeckSize() {
        Deck newDeck = new Deck();
        final int deckSize = 54;

        newDeck.drawCards(deckSize);
        assert newDeck.getDeckSize() == 0;
    }

    /**
     * Draw cards.
     */
    @Test
    void drawCards() {
        Deck newDeck = new Deck();
        final int deckSize = 54;

        List<Card> cardList = newDeck.drawCards(deckSize);
        assert newDeck.getDeckSize() == 0;
        assert cardList.size() == deckSize;
    }
}