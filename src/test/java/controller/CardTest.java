package controller;

import utils.CardAction;
import utils.CardType;
import utils.CardValue;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Card test.
 */
class CardTest {

    /**
     * Gets card action.
     */
    @org.junit.jupiter.api.Test
    void getCardAction() {
        Card newCard = new Card(CardValue.THREE, CardType.SPADES, CardAction.GET_THREE_CARDS);
        assert newCard.getCardAction().equals(CardAction.GET_THREE_CARDS);
    }

    /**
     * Gets card value.
     */
    @org.junit.jupiter.api.Test
    void getCardValue() {
        Card newCard = new Card(CardValue.THREE, CardType.SPADES, CardAction.GET_THREE_CARDS);
        assert newCard.getCardValue().equals(CardValue.THREE);
    }

    /**
     * Gets card type.
     */
    @org.junit.jupiter.api.Test
    void getCardType() {
        Card newCard = new Card(CardValue.THREE, CardType.SPADES, CardAction.GET_THREE_CARDS);
        assert newCard.getCardType().equals(CardType.SPADES);
    }

    /**
     * Sets card action.
     */
    @org.junit.jupiter.api.Test
    void setCardAction() {
        Card newCard = new Card();
        newCard.setCardAction(CardAction.NO_ACTION);
        assert newCard.getCardAction().equals(CardAction.NO_ACTION);
    }

    /**
     * Sets card value.
     */
    @org.junit.jupiter.api.Test
    void setCardValue() {
        Card newCard = new Card();
        newCard.setCardValue(CardValue.THREE);
        assert newCard.getCardValue().equals(CardValue.THREE);
    }

    /**
     * Sets card type.
     */
    @org.junit.jupiter.api.Test
    void setCardType() {
        Card newCard = new Card();
        newCard.setCardAction(CardAction.GET_THREE_CARDS);
        assert newCard.getCardAction().equals(CardAction.GET_THREE_CARDS);
    }
}