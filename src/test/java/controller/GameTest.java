package controller;

import model.User;
import org.junit.jupiter.api.Test;
import utils.CardType;
import utils.GameSettings;
import utils.GameStatus;

import java.awt.*;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Game test.
 */
class GameTest {

    /**
     * Gets current player.
     */
    @Test
    void getCurrentPlayer() {
        Player newPlayer = new Player(new User(1, "test", "pass"));
        Game newGame = new Game(1, "test", newPlayer);

        Player currentPlayer = newGame.getCurrentPlayer();
        assert currentPlayer.equals(newPlayer);
    }

    /**
     * Intitialize game.
     */
    @Test
    void intitializeGame() {
        Player newPlayer = new Player(new User(1, "test", "pass"));
        Player anotherPlayer = new Player(new User(2, "test", "pass"));
        Game newGame = new Game(1, "test", newPlayer);

        newGame.addPlayerToGame(anotherPlayer);
        boolean success = newGame.intitializeGame(newPlayer);
        assert success;

        Game failGame = new Game(1, "test", newPlayer);
        failGame.addPlayerToGame(anotherPlayer);
        Player notOwner = new Player(new User(2, "test", "pass"));
        boolean fail = failGame.intitializeGame(notOwner);

        assert !fail;
    }

    /**
     * Start game.
     */
    @Test
    void startGame() {
        Player newPlayer = new Player(new User(1, "test", "pass"));
        Player anotherPlayer = new Player(new User(2, "test", "pass"));
        Game newGame = new Game(1, "test", newPlayer);
        newGame.addPlayerToGame(anotherPlayer);

        newGame.intitializeGame(newPlayer);
        boolean success = newGame.startGame(newPlayer);
        assert success;

        Game failGame = new Game(1, "test", newPlayer);
        newGame.intitializeGame(newPlayer);
        failGame.addPlayerToGame(anotherPlayer);
        boolean fail = failGame.startGame(anotherPlayer);

        assert !fail;
    }

    /**
     * Add player to game.
     */
    @Test
    void addPlayerToGame() {
        Player newPlayer = new Player(new User(1, "test", "pass"));
        Player anotherPlayer = new Player(new User(2, "test", "pass"));
        Game newGame = new Game(1, "test", newPlayer);

        assert newGame.getJoinedPlayers().size() == 1;

        newGame.addPlayerToGame(anotherPlayer);
        assert newGame.getJoinedPlayers().size() == 2;

        newGame.intitializeGame(newPlayer);
        newGame.startGame(newPlayer);
        newGame.addPlayerToGame(new Player(new User(3, "Test", "pass")));

        assert newGame.getJoinedPlayers().size() == 2;
    }

    /**
     * Remove player from game.
     */
    @Test
    void removePlayerFromGame() {
        Player newPlayer = new Player(new User(1, "test", "pass"));
        Player anotherPlayer = new Player(new User(2, "test", "pass"));
        Game newGame = new Game(1, "test", newPlayer);

        assert newGame.getJoinedPlayers().size() == 1;

        newGame.addPlayerToGame(anotherPlayer);
        assert newGame.getJoinedPlayers().size() == 2;

        newGame.removePlayerFromGame(anotherPlayer);
        assert newGame.getJoinedPlayers().size() == 1;

        newGame.addPlayerToGame(anotherPlayer);
        newGame.intitializeGame(newPlayer);
        newGame.startGame(newPlayer);
        newGame.removePlayerFromGame(anotherPlayer);
        assert newGame.getJoinedPlayers().size() == 1;
    }

    /**
     * Terminate game.
     */
    @Test
    void terminateGame() {
        Player newPlayer = new Player(new User(1, "test", "pass"));
        Player anotherPlayer = new Player(new User(2, "test", "pass"));
        Game newGame = new Game(1, "test", newPlayer);
        newGame.addPlayerToGame(anotherPlayer);

        newGame.intitializeGame(newPlayer);
        newGame.startGame(newPlayer);
        assert newGame.getCurrentPlayer().getCards().size() == GameSettings.FIRST_DRAW_CARDS;
        assert newGame.getGameStatus().equals(GameStatus.STARTED);
        newGame.terminateGame();
        assert newGame.getCurrentPlayer().getCards().isEmpty();
        assert newGame.getGameStatus().equals(GameStatus.END);
    }

    /**
     * Place card.
     */
    @Test
    void placeCard() {
        Player newPlayer = new Player(new User(1, "test", "pass"));
        Player anotherPlayer = new Player(new User(2, "test", "pass"));
        Game newGame = new Game(1, "test", newPlayer);
        newGame.addPlayerToGame(anotherPlayer);

        newGame.intitializeGame(newPlayer);
        newGame.startGame(newPlayer);

        Card downCardType = newGame.getCurrentCard();
        Card playerCard = newPlayer.getCards().get(0);
        playerCard.setCardType(downCardType.getCardType());
        playerCard.setCardValue(downCardType.getCardValue());

        newGame.placeCard(newPlayer, newPlayer.discardToPile(playerCard, newGame.getCurrentCard()));
        if (newGame.getCurrentCard().getCardValue() == playerCard.getCardValue() ||
            newGame.getCurrentCard().getCardType() == playerCard.getCardType()) {
            assert newGame.getCurrentPlayer().equals(anotherPlayer) || !newGame.getCurrentPlayer().equals(anotherPlayer);
        } else {
            assert newGame.getCurrentPlayer().equals(newPlayer) || !newGame.getCurrentPlayer().equals(newPlayer);
        }
    }

    /**
     * Draw card.
     */
    @Test
    void drawCard() {
        Player newPlayer = new Player(new User(1, "test", "pass"));
        Player anotherPlayer = new Player(new User(2, "test", "pass"));
        Game newGame = new Game(1, "test", newPlayer);
        newGame.addPlayerToGame(anotherPlayer);

        newGame.intitializeGame(newPlayer);
        newGame.startGame(newPlayer);
        newGame.drawCard(newPlayer);
        assert newGame.getJoinedPlayers().get(0).getCards().size() == GameSettings.FIRST_DRAW_CARDS + 1;
        assert newGame.getCurrentPlayer().equals(anotherPlayer);
    }

    /**
     * Gets player by id.
     */
    @Test
    void getPlayerById() {
        Player newPlayer = new Player(new User(1, "test", "pass"));
        Player anotherPlayer = new Player(new User(2, "test", "pass"));
        Game newGame = new Game(1, "test", newPlayer);
        newGame.addPlayerToGame(anotherPlayer);

        Player idPlayer = newGame.getPlayerById(2);
        assert idPlayer.equals(anotherPlayer);
    }

    /**
     * Gets connected players.
     */
    @Test
    void getConnectedPlayers() {
        Player newPlayer = new Player(new User(1, "test", "pass"));
        Player anotherPlayer = new Player(new User(2, "test", "pass"));
        Game newGame = new Game(1, "test", newPlayer);
        newGame.addPlayerToGame(anotherPlayer);

        assert newGame.getConnectedPlayers() == 2;

        newGame.removePlayerFromGame(anotherPlayer);
        assert newGame.getConnectedPlayers() == 1;
    }

    /**
     * Gets current card.
     */
    @Test
    void getCurrentCard() {
        Player newPlayer = new Player(new User(1, "test", "pass"));
        Player anotherPlayer = new Player(new User(2, "test", "pass"));
        Game newGame = new Game(1, "test", newPlayer);
        newGame.addPlayerToGame(anotherPlayer);
        newGame.intitializeGame(newPlayer);
        newGame.startGame(newPlayer);

        Card currentCard = newGame.getCurrentCard();
        assert !currentCard.equals(new Card());
    }

    /**
     * Gets id.
     */
    @Test
    void getId() {
        Player newPlayer = new Player(new User(1, "test", "pass"));
        Game newGame = new Game(1, "test", newPlayer);

        assert newGame.getId() == 1;
    }

    /**
     * Sets id.
     */
    @Test
    void setId() {
        Player newPlayer = new Player(new User(1, "test", "pass"));
        Game newGame = new Game(1, "test", newPlayer);
        newGame.setId(2);

        assert newGame.getId() == 2;
    }

    /**
     * Gets name.
     */
    @Test
    void getName() {
        Player newPlayer = new Player(new User(1, "test", "pass"));
        Game newGame = new Game(1, "test", newPlayer);

        assert newGame.getName().equals("test");
    }

    /**
     * Sets name.
     */
    @Test
    void setName() {
        Player newPlayer = new Player(new User(1, "test", "pass"));
        Game newGame = new Game(1, "test", newPlayer);

        newGame.setName("newName");
        assert newGame.getName().equals("newName");
    }

    /**
     * Gets game status.
     */
    @Test
    void getGameStatus() {
        Player newPlayer = new Player(new User(1, "test", "pass"));
        Game newGame = new Game(1, "test", newPlayer);

        assert newGame.getGameStatus().equals(GameStatus.NOT_INITIALIZED);
    }

    /**
     * Gets owner.
     */
    @Test
    void getOwner() {
        Player newPlayer = new Player(new User(1, "test", "pass"));
        Game newGame = new Game(1, "test", newPlayer);

        assert newGame.getOwner().equals(newPlayer);
    }

    /**
     * Gets joined players.
     */
    @Test
    void getJoinedPlayers() {
        Player newPlayer = new Player(new User(1, "test", "pass"));
        Game newGame = new Game(1, "test", newPlayer);

        assert newGame.getJoinedPlayers().size() == 1;
    }

    /**
     * Test to string.
     */
    @Test
    void testToString() {
        Player newPlayer = new Player(new User(1, "test", "pass"));
        Game newGame = new Game(1, "test", newPlayer);

        assert newGame.toString().equals("Game{" + "id=" + newGame.getId() + ", name='" + newGame.getName() + '\'' + '}');
    }
}