package database;

import org.junit.jupiter.api.Test;

import java.sql.Connection;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Connection db test.
 */
class ConnectionDBTest {

    /**
     * Gets db connection instance.
     */
    @Test
    void getDBConnectionInstance() {
        ConnectionDB dbInstance = ConnectionDB.getDBConnectionInstance();

        assert dbInstance != null;
    }

    /**
     * Gets db connection.
     */
    @Test
    void getDbConnection() {
        Connection dbConnection = null;
        dbConnection = ConnectionDB.getDBConnectionInstance().getDbConnection();

        assert dbConnection != null;
    }
}