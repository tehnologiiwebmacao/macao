package database;

import model.User;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Connector test.
 */
class ConnectorTest {

    /**
     * Gets accounts.
     */
    @Test
    void getAccounts() {
        ArrayList<User> users = Connector.getAccounts();

        assert !users.isEmpty();
    }

    /**
     * Register.
     */
    @Test
    void register() {
        User newUser = Connector.register("test", "pass");

        assert newUser.getUsername().equals("test") && newUser.getPassword().equals("pass");
    }
}