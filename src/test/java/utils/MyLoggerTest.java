package utils;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.logging.Level;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type My logger test.
 */
class MyLoggerTest {

    /**
     * Gets instance.
     *
     * @throws IOException the io exception
     */
    @Test
    void getInstance() throws IOException {
        assert MyLogger.getInstance() != null;
    }

    /**
     * Log.
     *
     * @throws IOException the io exception
     */
    @Test
    void log() throws IOException {
        MyLogger.getInstance().log(Level.INFO, "test");
        assert true;
    }
}