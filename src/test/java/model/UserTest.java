package model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type User test.
 */
class UserTest {

    /**
     * Gets id user.
     */
    @Test
    void getIdUser() {
        User newUser = new User(1, "test", "pass");

        assert newUser.getIdUser() == 1;
    }

    /**
     * Sets id user.
     */
    @Test
    void setIdUser() {
        User newUser = new User(1, "test", "pass");

        newUser.setIdUser(2);
        assert newUser.getIdUser() == 2;
    }

    /**
     * Gets username.
     */
    @Test
    void getUsername() {
        User newUser = new User(1, "test", "pass");

        assert newUser.getUsername().equals("test");
    }

    /**
     * Sets username.
     */
    @Test
    void setUsername() {
        User newUser = new User(1, "test", "pass");

        newUser.setUsername("newName");
        assert newUser.getUsername().equals("newName");
    }

    /**
     * Gets password.
     */
    @Test
    void getPassword() {
        User newUser = new User(1, "test", "pass");

        assert newUser.getPassword().equals("pass");
    }

    /**
     * Sets password.
     */
    @Test
    void setPassword() {
        User newUser = new User(1, "test", "pass");

        newUser.setPassword("newPassword");
        assert newUser.getPassword().equals("newPassword");
    }

    /**
     * Test to string.
     */
    @Test
    void testToString() {
        User newUser = new User(1, "test", "pass");

        assert newUser.toString().equals("Users [iduser= " + newUser.getIdUser()
                + ", password= " + newUser.getPassword() + ", username= "
                + newUser.getUsername() + "]");
    }
}