package database;

import utils.MyLogger;

import java.sql.*;
import java.util.logging.Level;

/**
 * Singleton class that instances the connection to the DB.
 */
class ConnectionDB {
    private static final String URL = "jdbc:mysql://localhost:3306/login?autoReconnect=true&useSSL=false";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "1q2w3e";

    private Connection dbConnection = null;
    private static ConnectionDB connectionDbInstance = null;

    /**
     * Gets db connection instance.
     *
     * @return the db connection instance
     */
    static ConnectionDB getDBConnectionInstance() {
        try {
            if (connectionDbInstance == null) {
                connectionDbInstance = new ConnectionDB();
            } else if (connectionDbInstance.getDbConnection().isClosed()) {
                connectionDbInstance = new ConnectionDB();
            }
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
        return connectionDbInstance;
    }

    private ConnectionDB() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            dbConnection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            MyLogger.getInstance().log(Level.INFO, "Established connection to data base");

        } catch (SQLException e) {
            e.printStackTrace();
            System.err.println(e.getMessage());
        } catch (Exception e) {
            MyLogger.getInstance().log(Level.SEVERE, "Failed connection to data base");
            e.printStackTrace();
        }
    }

    /**
     * Gets db connection.
     *
     * @return the db connection
     */
    Connection getDbConnection() {
        return dbConnection;
    }
}
