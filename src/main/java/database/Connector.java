package database;

import model.User;
import utils.MyLogger;

import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;

/**
 * Class that handles the connection to data base and executes SQL statements
 */
public class Connector {
    /**
     * Gets the users from DB.
     * @return users
     */
    public static synchronized ArrayList<User> getAccounts() {
        Connection dbConnection = ConnectionDB.getDBConnectionInstance().getDbConnection();

        ArrayList<User> users = new ArrayList<>();
        try {
            Statement dbStatement = dbConnection.createStatement();
            ResultSet resultSet = dbStatement.executeQuery("SELECT * FROM user");

            while (resultSet.next()) {
                Integer id = resultSet.getInt("iduser");
                String username = resultSet.getString("username");
                String password = resultSet.getString("password");
                users.add(new User(id, username, password));

                System.out.println(id);
            }
            if (users.isEmpty()) {
                System.out.println("No Registered Users in Data Base");
            }
            resultSet.close();
            dbStatement.close();
        } catch (SQLException e) {
            MyLogger.getInstance().log(Level.SEVERE, "Fail SQL statement");
            e.printStackTrace();
        }
        MyLogger.getInstance().log(Level.INFO, "Retrieved Users from Data Base");
        return users;
    }

    /**
     * Registers the user to the DB with the given credentials.
     * @param username username of the new user
     * @param password password of the new user
     * @return user
     */
    public static synchronized User register(String username, String password) {
        Connection dbConnection = ConnectionDB.getDBConnectionInstance().getDbConnection();
        ArrayList<User> userArrayList = getAccounts();
        Integer newUserId = userArrayList.size() + 1;
        User newUser = new User(newUserId, username, password);
        try {
            Statement myState = ConnectionDB.getDBConnectionInstance().getDbConnection().createStatement();
            String sqlInsert = "INSERT INTO user VALUES('" + newUserId.toString() + "','" + username + "','" + password + "')";
            myState.executeUpdate(sqlInsert);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return newUser;
    }

}
