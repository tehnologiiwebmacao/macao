package controller;

import model.User;
import utils.CardAction;
import utils.CardType;
import utils.PlayerStatus;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Player.
 */
public class Player {
    private User user;
    private ArrayList<Card> cards;
    private Integer noWins;
    private PlayerStatus status;

    /**
     * Instantiates a new Player.
     *
     * @param user the user
     */
    public Player(User user) {
        this.user = user;
        this.cards = new ArrayList<>();

        noWins = 0;
        status = PlayerStatus.SPECTATING;
    }

    /**
     * Give cards.
     *
     * @param cardsToGive the cards to give
     */
    public void giveCards(List<Card> cardsToGive) {
        this.cards.addAll(cardsToGive);
    }

    /**
     * Give card.
     *
     * @param cardToGive the card to give
     */
    public void giveCard(Card cardToGive) {
        this.cards.add(cardToGive);
    }

    /**
     * Discard to pile card.
     *
     * @param selectedCard the selected card
     * @param downCard     the down card
     * @return the card
     */
    public Card discardToPile(Card selectedCard, Card downCard) {
        int indexOfCard = this.cards.indexOf(selectedCard);
        if (indexOfCard != -1) {
            Card card = this.cards.get(indexOfCard);
                return this.cards.remove(this.cards.indexOf(selectedCard));

        }
        return new Card();
    }

    /**
     * Discard all.
     */
    public void discardAll() {
        cards.clear();
    }

    /**
     * Gets hand size.
     *
     * @return the hand size
     */
    public int getHandSize() {
        if (!this.cards.isEmpty()) {
            return this.cards.size();
        }
        return 0;
    }

    /**
     * Increment win.
     */
    public void incrementWin() {
        this.noWins++;
    }

    /**
     * Has stop or get boolean.
     *
     * @return the boolean
     */
    public boolean hasStopOrGet() {
        if (!this.cards.isEmpty()) {
            Card found = this.cards.stream().filter(card -> card.getCardAction() != CardAction.NO_ACTION &&
                    card.getCardAction() != CardAction.CHANGE_TYPE).findAny().orElse(null);
            return found != null;
        }
        return false;
    }

    /**
     * Gets cards.
     *
     * @return the cards
     */
    public List<Card> getCards() {
        return this.cards;
    }

    /**
     * Gets user.
     *
     * @return the user
     */
    public User getUser() {
        return this.user;
    }

    /**
     * Gets no wins.
     *
     * @return the no wins
     */
    public Integer getNoWins() {
        return noWins;
    }

    /**
     * Gets status.
     *
     * @return the status
     */
    public PlayerStatus getStatus() {
        return status;
    }

    /**
     * Sets status.
     *
     * @param status the status
     */
    public void setStatus(PlayerStatus status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Player)) {
            return false;
        }
        Player player = (Player) obj;

        return this.user.getIdUser().intValue() == player.user.getIdUser().intValue();
    }
}
