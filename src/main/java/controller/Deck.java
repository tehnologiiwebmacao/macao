package controller;

import utils.CardAction;
import utils.CardType;
import utils.CardValue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The type Deck.
 */
public class Deck {
    private ArrayList<Card> deck;

    /**
     * Instantiates a new Deck.
     */
    public Deck(){
        deck = new ArrayList<>();
        reset();
    }

    /**
     * Reset.
     */
    public void reset() {
        deck.clear();

        for(CardValue cardValue : CardValue.values())
            for(CardType cardType : CardType.values())
                if(!cardValue.equals(CardValue.INVALID_VALUE) &&
                        !cardValue.equals(CardValue.JOKER) &&
                        !cardType.equals(CardType.RED) &&
                        !cardType.equals(CardType.BLACK) &&
                        !cardType.equals(CardType.INVALID_TYPE))
                    deck.add(new Card(cardValue, cardType, CardAction.NO_ACTION));

        generateSpecialCards();

        Collections.shuffle(deck);
    }

    private void generateSpecialCards(){
        deck.add(new Card(CardValue.JOKER, CardType.RED, CardAction.GET_TEN_CARDS));
        deck.add(new Card(CardValue.JOKER, CardType.BLACK, CardAction.GET_FIVE_CARDS));

        for(Card card: deck){
            switch (card.getCardValue()){
                case TWO:
                    card.setCardAction(CardAction.GET_TWO_CARDS);
                    break;
                case THREE:
                    card.setCardAction(CardAction.GET_THREE_CARDS);
                    break;
                case FOUR:
                    card.setCardAction(CardAction.STOP);
                    break;
                case SEVEN:
                    card.setCardAction(CardAction.CHANGE_TYPE);
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * Draw card card.
     *
     * @return the card
     */
    public Card drawCard() {
        if (!deck.isEmpty()) {
            return deck.remove(this.deck.size() - 1);
        }
        return null;
    }

    /**
     * Gets deck size.
     *
     * @return the deck size
     */
    public int getDeckSize() {
        if (!this.deck.isEmpty())
            return this.deck.size();
        return 0;
    }

    /**
     * Draw cards list.
     *
     * @param numberOfCardsToDarw the number of cards to darw
     * @return the list
     */
    public List<Card> drawCards(Integer numberOfCardsToDarw) {
        List<Card> cardsToReturn = new ArrayList<>();
        for (int i = 1; i <= numberOfCardsToDarw; i++) {
            if (!deck.isEmpty()) {
                 cardsToReturn.add(deck.remove(this.deck.size() - 1));
            }
        }
        return cardsToReturn;
    }
}
