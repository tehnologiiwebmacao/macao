package controller;

import utils.CardAction;
import utils.CardType;
import utils.CardValue;

/**
 * The type Card.
 */
public class Card {
    private CardValue cardValue;
    private CardType cardType;
    private CardAction cardAction;

    /**
     * Sets card action.
     *
     * @param cardAction the card action
     */
    public void setCardAction(CardAction cardAction) {
        this.cardAction = cardAction;
    }

    /**
     * Gets card value.
     *
     * @return the card value
     */
    public CardValue getCardValue() {
        return cardValue;
    }

    /**
     * Sets card value.
     *
     * @param cardValue the card value
     */
    public void setCardValue(CardValue cardValue) {
        this.cardValue = cardValue;
    }

    /**
     * Sets card type.
     *
     * @param cardType the card type
     */
    public void setCardType(CardType cardType) {
        this.cardType = cardType;
    }

    /**
     * Gets card type.
     *
     * @return the card type
     */
    public CardType getCardType() {
        return cardType;
    }

    /**
     * Gets card action.
     *
     * @return the card action
     */
    public CardAction getCardAction() {
        return cardAction;
    }

    /**
     * Instantiates a new Card.
     *
     * @param cardValue  the card value
     * @param cardType   the card type
     * @param cardAction the card action
     */
    public Card(CardValue cardValue, CardType cardType, CardAction cardAction) {
        this.cardValue = cardValue;
        this.cardType = cardType;
        this.cardAction = cardAction;
    }

    /**
     * Instantiates a new Card.
     */
    public Card() {
        this.cardValue = CardValue.INVALID_VALUE;
        this.cardType = CardType.INVALID_TYPE;
        this.cardAction = CardAction.INVALID_ACTION;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Card)) {
            return false;
        }
        Card card = (Card) obj;

        return this.getCardType() == card.getCardType() && this.getCardValue() == card.getCardValue() &&
                this.getCardAction() == card.getCardAction();
    }
}
