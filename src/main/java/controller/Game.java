package controller;

import utils.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

/**
 * The type Game.
 */
public class Game implements Serializable {
    private Integer id;
    private String name;

    private Deck deck;

    private Card downCard;
    private Integer previousIncrement;

    private Player owner;

    /**
     * Gets current player.
     *
     * @return the current player
     */
    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    private Player currentPlayer;
    private List<Player> joinedPlayers;
    private Integer currentPlayerIndex;

    private GameStatus gameStatus;

    /**
     * Instantiates a new Game.
     *
     * @param id    the id
     * @param name  the name
     * @param owner the owner
     */
    public Game(Integer id, String name, Player owner) {
        this.id = id;
        this.name = name;
        this.owner = owner;
        this.currentPlayer = owner;
        this.gameStatus = GameStatus.NOT_INITIALIZED;
        this.joinedPlayers = new ArrayList<Player>() {{
            add(owner);
        }};
        this.currentPlayerIndex = this.joinedPlayers.indexOf(owner);

        this.previousIncrement = 0;
        this.deck = null;
        this.downCard = null;
    }

    /**
     * Intitialize game boolean.
     *
     * @param requestFromPlayer the request from player
     * @return the boolean
     */
    public boolean intitializeGame(Player requestFromPlayer) {
        if (gameStatus == GameStatus.NOT_INITIALIZED || gameStatus == GameStatus.END) {
            if (requestFromPlayer == owner) {
                if (this.joinedPlayers.size() >= GameSettings.MINIMUM_PLAYERS) {
                    this.deck = new Deck();
                    this.downCard = deck.drawCard();
                    for (Player player : joinedPlayers) {
                        player.giveCards(this.deck.drawCards(GameSettings.FIRST_DRAW_CARDS));
                    }
                    this.gameStatus = GameStatus.INITIALIZED;
                    MyLogger.getInstance().log(Level.INFO, "Game wa initialized");
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Start game boolean.
     *
     * @param requestFromPlayer the request from player
     * @return the boolean
     */
    public boolean startGame(Player requestFromPlayer) {
        if (gameStatus == GameStatus.INITIALIZED) {
            if (requestFromPlayer == owner) {
                MyLogger.getInstance().log(Level.INFO, "Game logic started");
                this.gameStatus = GameStatus.STARTED;
                return true;
            }
        }
        return false;
    }

    /**
     * Add player to game.
     *
     * @param newPlayer the new player
     */
    public void addPlayerToGame(Player newPlayer) {
        if ((this.gameStatus != GameStatus.STARTED) && (this.gameStatus != GameStatus.INITIALIZED)) {
            if (this.joinedPlayers.size() <= GameSettings.MAXIMUM_PLAYERS) {
                if (!this.joinedPlayers.contains(newPlayer)) {
                    MyLogger.getInstance().log(Level.INFO, "Player added to game logic");
                    this.joinedPlayers.add(newPlayer);
                }
            }
        }
    }

    /**
     * Remove player from game.
     *
     * @param playerToRemove the player to remove
     */
//Call This When a player left
    public void removePlayerFromGame(Player playerToRemove) {
        if ((this.gameStatus != GameStatus.STARTED) && (this.gameStatus != GameStatus.INITIALIZED)) {
            if (!this.joinedPlayers.isEmpty()) {
                MyLogger.getInstance().log(Level.INFO, "Player removed from game logic");
                this.joinedPlayers.remove(playerToRemove);
                if (playerToRemove == owner) {
                    if (!this.joinedPlayers.isEmpty()) {
                        this.owner = this.joinedPlayers.get(this.joinedPlayers.size() - 1);
                    } else {
                        terminateGame();
                    }
                }
            }
        } else {
            this.joinedPlayers.remove(playerToRemove);
            terminateGame();
        }
    }

    /**
     * Terminate game.
     */
//Call This When a player left, is not responding, there are no more cards in deck
    public void terminateGame() {
        MyLogger.getInstance().log(Level.INFO, "Game ended");
        this.gameStatus = GameStatus.END;
        if (!this.joinedPlayers.isEmpty()) {
            for (Player player : joinedPlayers) {
                player.discardAll();
            }
        }
        this.deck = null;
        this.downCard = null;
    }

    private boolean isCardPlayable(Card selectedCard) {
        if (downCard.getCardAction() == CardAction.NO_ACTION || downCard.getCardAction() == CardAction.STOP || downCard.getCardAction() == CardAction.CHANGE_TYPE) {
            if (downCard.getCardValue() == selectedCard.getCardValue() || downCard.getCardType() == selectedCard.getCardType())
                return true;
        }

        if (selectedCard.getCardAction() == CardAction.GET_FIVE_CARDS || selectedCard.getCardAction() == CardAction.GET_TEN_CARDS)
            return true;

        if (previousIncrement == 0) {
            if (downCard.getCardAction() == CardAction.GET_FIVE_CARDS || downCard.getCardAction() == CardAction.GET_TEN_CARDS)
                return true;
            if (downCard.getCardValue() == selectedCard.getCardValue() || downCard.getCardType() == selectedCard.getCardType())
                return true;
        }

        if (previousIncrement != 0) {
            if (downCard.getCardAction() == CardAction.GET_TWO_CARDS ||
                    downCard.getCardAction() == CardAction.GET_THREE_CARDS ||
                    downCard.getCardAction() == CardAction.GET_FIVE_CARDS ||
                    downCard.getCardAction() == CardAction.GET_TEN_CARDS)
                if (selectedCard.getCardAction() != CardAction.NO_ACTION && selectedCard.getCardAction() != CardAction.CHANGE_TYPE)
                    return true;
        }

        if(downCard.getCardValue() == CardValue.SEVEN){
            return true;
        }

        return false;
    }

    //Call This When Click |PLACE CARD|
    public void placeCard(Player playerRequest, Card selectedCard) {
        if (this.currentPlayer.equals(playerRequest)) {
            if (this.currentPlayer.getHandSize() != 0) {
                if (isCardPlayable(selectedCard)) {
                    this.downCard = this.currentPlayer.discardToPile(selectedCard, this.downCard);
                    checkSpecialCard();
                    endTurn();
                    checkWinner();
                }
            }
        }
    }

    /**
     * Draw card.
     *
     * @param playerRequest the player request
     */
//Call This When Click |DRAW CARD|
    public void drawCard(Player playerRequest) {
        if (this.gameStatus == GameStatus.STARTED) {
            if (this.deck.getDeckSize() != 0) {
                if (this.currentPlayer.equals(playerRequest)) {
                    MyLogger.getInstance().log(Level.INFO, "Card drawn");
                    this.currentPlayer.giveCard(this.deck.drawCard());
                    endTurn();
                }
            } else {
                terminateGame();
            }
        }
    }

    private void endTurn() {
        MyLogger.getInstance().log(Level.INFO, "Turn ended");
        this.currentPlayerIndex++;
        if (this.currentPlayerIndex >= this.joinedPlayers.size()) {
            this.currentPlayerIndex = 0;
        }
        this.currentPlayer = this.joinedPlayers.get(this.currentPlayerIndex);
        handlePreviousSpecialCard();
    }

    private void checkWinner() {
        for (Player player : this.joinedPlayers) {
            if (player.getHandSize() == 0) {
                MyLogger.getInstance().log(Level.INFO, "Winner found");
                player.incrementWin();
                terminateGame();
                break;
            }
        }
    }

    private void handlePreviousSpecialCard() {
        if (!this.currentPlayer.hasStopOrGet()) {
            switch (this.downCard.getCardAction()) {
                case NO_ACTION:
                    previousIncrement = 0;
                    break;
                case GET_TEN_CARDS:
                    this.currentPlayer.giveCards(this.deck.drawCards( previousIncrement));
                    previousIncrement = 0;
                    break;
                case GET_FIVE_CARDS:
                    this.currentPlayer.giveCards(this.deck.drawCards( previousIncrement));
                    previousIncrement = 0;
                    break;
                case GET_THREE_CARDS:
                    this.currentPlayer.giveCards(this.deck.drawCards( previousIncrement));
                    previousIncrement = 0;
                    break;
                case GET_TWO_CARDS:
                    this.currentPlayer.giveCards(this.deck.drawCards(previousIncrement));
                    previousIncrement = 0;
                    break;
                default:
                    previousIncrement = 0;
                    break;
            }
        }
    }

    private void checkSpecialCard() {
        switch (this.downCard.getCardAction()) {
            case NO_ACTION:
                break;
            case GET_TEN_CARDS:
                this.previousIncrement += 10;
                break;
            case GET_FIVE_CARDS:
                this.previousIncrement += 5;
                break;
            case GET_THREE_CARDS:
                this.previousIncrement += 3;
                break;
            case GET_TWO_CARDS:
                this.previousIncrement += 2;
                break;
            case STOP:
                this.previousIncrement = 0;
                break;
            case CHANGE_TYPE:
                //IDK what to do here, just change the current card to other Type
                break;
            default:
                break;
        }
    }


    /**
     * Gets player by id.
     *
     * @param playerId the player id
     * @return the player by id
     */
    public Player getPlayerById(int playerId) {
        return this.joinedPlayers.stream().filter(player -> player.getUser().getIdUser() == playerId)
                .findAny().orElse(null);
    }

    /**
     * Gets connected players.
     *
     * @return the connected players
     */
    public Integer getConnectedPlayers() {
        return this.joinedPlayers.size();
    }


    /**
     * Gets current card.
     *
     * @return the current card
     */
    public Card getCurrentCard() {
        return this.downCard;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets game status.
     *
     * @return the game status
     */
    public GameStatus getGameStatus() {
        return this.gameStatus;
    }

    /**
     * Gets owner.
     *
     * @return the owner
     */
    public Player getOwner() {
        return this.owner;
    }

    /**
     * Gets joined players.
     *
     * @return the joined players
     */
    public List<Player> getJoinedPlayers()
    {
        return joinedPlayers;
    }

    @Override
    public String toString() {
        return "Game{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
