package servlet;

import database.Connector;
import model.User;
import utils.MyLogger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.logging.Level;

/**
 * The type Login.
 */
@WebServlet(name = "login",
        urlPatterns = {"/login"},
        loadOnStartup = 1)
public class Login extends HttpServlet {

    /**
     * Instantiates a new Login.
     */
    public Login() {
        super();
    }

    private void setHeadNoCache(HttpServletRequest req, HttpServletResponse resp) {
        resp.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        resp.setHeader("Pragma", "no-cache");
        resp.setHeader("Expires", "0");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setHeadNoCache(req, resp);
        String username = req.getParameter("username");
        String password = req.getParameter("password");

        User found = Connector.getAccounts().stream().filter(user -> username.equals(user.getUsername())
                && password.equals(user.getPassword())).findAny().orElse(null);

        if (username.isEmpty() || password.isEmpty()) {
            MyLogger.getInstance().log(Level.WARNING, "Data not existing for login");
            RequestDispatcher newReq = req.getRequestDispatcher("login.jsp");
            newReq.include(req, resp);
        } else {
            if (found != null) {
                MyLogger.getInstance().log(Level.INFO, "User found, redirecting to home page");
                ServletContext context = getServletContext();
                RequestDispatcher rd = context.getRequestDispatcher("/home");
                HttpSession newSession = req.getSession();
                newSession.setAttribute("user", found.getUsername());
                newSession.setAttribute("password", found.getPassword());
                newSession.setAttribute("id", found.getIdUser());
                rd.forward(req,resp);
            } else {
                MyLogger.getInstance().log(Level.WARNING, "User not found in data base");
                RequestDispatcher re = req.getRequestDispatcher("login.jsp");
                re.include(req, resp);
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    }
}
