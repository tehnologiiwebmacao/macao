package servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import controller.Game;
import controller.Player;
import model.User;
import controller.Lobby;
import utils.MyLogger;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

/**
 * The type Home.
 */
@WebServlet(name = "home",
        urlPatterns = {"/home"},
        loadOnStartup = 1)
public class Home extends HttpServlet {

    @Override
    public void init() throws ServletException {
        Lobby.games = new ArrayList<>();
    }

    private List<String> getDynamicButtons() {
        List<String> buttons = new ArrayList<>();
        for (Game game : Lobby.games) {
            buttons.add("button" + game.getId());
        }
        return buttons;
    }

    private void setHeadNoCache(HttpServletRequest req, HttpServletResponse resp) {
        resp.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        resp.setHeader("Pragma", "no-cache");
        resp.setHeader("Expires", "0");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setHeadNoCache(req, resp);
        HttpSession session = req.getSession(false);
        if (session != null) {
            String username = (String)session.getAttribute("user");
            String password = (String)session.getAttribute("password");
            int id = (int)session.getAttribute("id");

            if (req.getParameter("logout") != null) { // Check Log Out
                MyLogger.getInstance().log(Level.INFO, "Player left the game");
                RequestDispatcher re = req.getRequestDispatcher("login.jsp");
                req.getSession().invalidate();
                re.include(req, resp);
            } else if (req.getParameter("addGame") != null) { // Check Add Game
                MyLogger.getInstance().log(Level.INFO, "Player added a new game");
                String gameName = req.getParameter("gameName");
                if (gameName != null) {
                    if (!gameName.isEmpty()) {
                        RequestDispatcher re = req.getRequestDispatcher("/game");

                        Lobby.games.add(new Game(Lobby.games.size() + 1, gameName, new Player(new User(id, username, password))));

                        session.setAttribute("gameNumber", Lobby.games.size() - 1);
                        session.setAttribute("joinedPlayer", new Player(new User(id, username, password)));

                        re.forward(req, resp);
                    }
                }
            } else { // Check Join game
                MyLogger.getInstance().log(Level.INFO, "Player joined the game");
                List<String> buttons = getDynamicButtons();
                for (String button : buttons) {
                    if (req.getParameter(button) != null) {
                        // TODO Don't redirect player to the game page if the room is full
                        RequestDispatcher re = req.getRequestDispatcher("/game");
                        Player joinedPlayer = new Player(new User(id, username, password));
                        int gameNumber = Integer.parseInt(button.substring(button.length() - 1, button.length())) - 1;

                        if(Lobby.games.get(gameNumber).getJoinedPlayers().size() < 6) {
                            Lobby.games.get(gameNumber).addPlayerToGame(joinedPlayer);
                            session.setAttribute("gameNumber", gameNumber);
                            session.setAttribute("joinedPlayer", new Player(new User(id, username, password)));

                            re.forward(req, resp);
                        }
                    }
                }
            }
            RequestDispatcher re = req.getRequestDispatcher("home.jsp");
            req.getSession().setAttribute("listGames", Lobby.games);
            re.include(req, resp);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        mapper.writeValue(resp.getWriter(), Lobby.games);
        resp.getWriter().flush();
        resp.getWriter().close();
    }
}
