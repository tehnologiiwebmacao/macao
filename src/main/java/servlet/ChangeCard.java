package servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import controller.Card;
import controller.Lobby;
import controller.Player;
import utils.CardType;
import utils.CardValue;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Game.
 */
@WebServlet(name = "changeCard",
        urlPatterns = {"/changeCard"},
        loadOnStartup = 1)
public class ChangeCard extends HttpServlet {

    private String cardType;


    private void setHeadNoCache(HttpServletRequest req, HttpServletResponse resp) {
        resp.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        resp.setHeader("Pragma", "no-cache");
        resp.setHeader("Expires", "0");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setHeadNoCache(req, resp);
        HttpSession session = req.getSession(false);
        if (session != null) {

            if(req.getParameter("cardType") != null)
            {
                cardType = req.getParameter("cardType");
                session.setAttribute("changedCardType", req.getParameter("cardType"));
                System.out.println(req.getParameter("cardType"));
            }

            RequestDispatcher re = req.getRequestDispatcher("game.jsp");
            re.include(req, resp);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        session.setAttribute("changedCardType", cardType);

        ObjectMapper mapper = new ObjectMapper();
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        mapper.writeValue(resp.getWriter(), cardType);
        resp.getWriter().flush();
        resp.getWriter().close();
    }
}

