package servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import controller.Card;
import controller.Lobby;
import controller.Player;
import utils.CardType;
import utils.CardValue;
import utils.MyLogger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

/**
 * The type Game.
 */
@WebServlet(name = "game",
        urlPatterns = {"/game"},
        loadOnStartup = 1)
public class Game extends HttpServlet {

    private List<String> getDynamicButtons(Player joinedPlayer) {
        List<String> buttons = new ArrayList<>();
        int cardNo = 0;
        for (Card card : joinedPlayer.getCards()) {
            buttons.add("button" + cardNo++);
        }
        return buttons;
    }

    private void setHeadNoCache(HttpServletRequest req, HttpServletResponse resp) {
        resp.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        resp.setHeader("Pragma", "no-cache");
        resp.setHeader("Expires", "0");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setHeadNoCache(req, resp);
        HttpSession session = req.getSession(false);
        if (session != null) {
            int gameNumber = (int) session.getAttribute("gameNumber");
            int joinedPlayerId = ((Player) session.getAttribute("joinedPlayer")).getUser().getIdUser();
            controller.Game pageGame = Lobby.games.get(gameNumber);// to get the reference for the game
            Player joinedPlayer = pageGame.getPlayerById(joinedPlayerId); //to get the reference for the player

            session.setAttribute("playerCards", joinedPlayer.getCards());
            session.setAttribute("downCard", pageGame.getCurrentCard());

            // ----------------------GAME LOGIC-------------------------------------------------------------------------

            // make this possible only when the start button was pressed
//            pageGame.intitializeGame(joinedPlayer); //only initializes if the player is owner

            // start a timer for 60 seconds
//            pageGame.startGame(joinedPlayer); //only initializes if the player is owner

            // if player pressed on a card
//            pageGame.placeCard(joinedPlayer, /*card button from jsp*/); //ends the turn automatically

            // if player pressed draw card
//            pageGame.drawCard(joinedPlayer);

            // if the timer is on 0 (the time passed), check if the pagePlayer is the one guilty
            // if he is the one, then call removePlayerFromGame()
//            pageGame.removePlayerFromGame(joinedPlayer);

            // ----------------------GAME LOGIC-------------------------------------------------------------------------


            if (req.getParameter("logout") != null) { // Check Log Out
                MyLogger.getInstance().log(Level.INFO, "Player left the game");
                RequestDispatcher re = req.getRequestDispatcher("login.jsp");
                pageGame.removePlayerFromGame(joinedPlayer);
                session.invalidate();
                re.include(req, resp);
            } else if (req.getParameter("drawCard") != null) {
                MyLogger.getInstance().log(Level.INFO, "Player drew a card");
                pageGame.drawCard(joinedPlayer);
            } else if (req.getParameter("startGame") != null) {
                pageGame.intitializeGame(joinedPlayer);
                MyLogger.getInstance().log(Level.INFO, "Game was initialized");
                pageGame.startGame(joinedPlayer);
                MyLogger.getInstance().log(Level.INFO, "Game started");
            } else {
                List<String> cardButtons = getDynamicButtons(joinedPlayer);
                for (String cardButton : cardButtons) {
                    if (req.getParameter(cardButton) != null) {
                        MyLogger.getInstance().log(Level.INFO, "A card was clicked");
                        int index = Integer.parseInt(cardButton.substring(cardButton.length() - 1, cardButton.length()));
                        if (pageGame.getCurrentCard().getCardValue() == CardValue.SEVEN) {
                            String type = (String) session.getAttribute("changedCardType");

                            System.out.println(type + "   " + joinedPlayer.getCards().get(index).getCardType().toString());
                            if (type != null && type.equals(joinedPlayer.getCards().get(index).getCardType().toString())) {
                                pageGame.placeCard(joinedPlayer, joinedPlayer.getCards().get(index));
                                System.out.println(cardButton + " " + index);
                            }
                        } else {
                            pageGame.placeCard(joinedPlayer, joinedPlayer.getCards().get(index));
                            System.out.println(cardButton + " " + index);
                        }
                    }
                }
            }

            if (req.getParameter("cardType") != null) {
                session.setAttribute("changedCardType", req.getParameter("cardType"));
                System.out.println(req.getParameter("cardType"));
            }

            session.setAttribute("currentPlayer", joinedPlayer);
            session.setAttribute("game", pageGame);

            RequestDispatcher re = req.getRequestDispatcher("game.jsp");
            re.include(req, resp);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        int gameNumber = (int) session.getAttribute("gameNumber");
        int joinedPlayerId = ((Player) session.getAttribute("joinedPlayer")).getUser().getIdUser();
        controller.Game pageGame = Lobby.games.get(gameNumber);// to get the reference for the game
        Player joinedPlayer = pageGame.getPlayerById(joinedPlayerId); //to get the reference for the player

        session.setAttribute("currentPlayer", pageGame.getCurrentPlayer());
        session.setAttribute("game", pageGame);

        ArrayList<Card> cards = new ArrayList<Card>();
        if (pageGame.getCurrentCard().getCardType() != CardType.INVALID_TYPE && pageGame.getCurrentCard().getCardValue() != CardValue.INVALID_VALUE)
            cards.add(pageGame.getCurrentCard());
        else
            cards.add(null);
        cards.addAll(joinedPlayer.getCards());

        ObjectMapper mapper = new ObjectMapper();
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        mapper.writeValue(resp.getWriter(), cards);
        resp.getWriter().flush();
        resp.getWriter().close();
    }
}
