package utils;

/**
 * The enum Card value.
 */
public enum CardValue {
    /**
     * Invalid value card value.
     */
    INVALID_VALUE,
    /**
     * Ace card value.
     */
    ACE,
    /**
     * Two card value.
     */
    TWO,
    /**
     * Three card value.
     */
    THREE,
    /**
     * Four card value.
     */
    FOUR,
    /**
     * Five card value.
     */
    FIVE,
    /**
     * Six card value.
     */
    SIX,
    /**
     * Seven card value.
     */
    SEVEN,
    /**
     * Eight card value.
     */
    EIGHT,
    /**
     * Nine card value.
     */
    NINE,
    /**
     * Ten card value.
     */
    TEN,
    /**
     * Jack card value.
     */
    JACK,
    /**
     * Queen card value.
     */
    QUEEN,
    /**
     * King card value.
     */
    KING,
    /**
     * Joker card value.
     */
    JOKER
}
