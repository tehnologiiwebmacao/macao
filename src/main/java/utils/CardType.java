package utils;

/**
 * The enum Card type.
 */
public enum CardType {
    /**
     * Invalid type card type.
     */
    INVALID_TYPE,
    /**
     * Hearts card type.
     */
    HEARTS,
    /**
     * Diamonds card type.
     */
    DIAMONDS,
    /**
     * Clubs card type.
     */
    CLUBS,
    /**
     * Spades card type.
     */
    SPADES,
    /**
     * Red card type.
     */
    RED,
    /**
     * Black card type.
     */
    BLACK
}
