package utils;

/**
 * The enum Player status.
 */
public enum PlayerStatus {
    /**
     * Playing player status.
     */
    PLAYING,
    /**
     * Spectating player status.
     */
    SPECTATING
}
