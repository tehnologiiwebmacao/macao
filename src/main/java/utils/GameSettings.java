package utils;

/**
 * The type Game settings.
 */
public class GameSettings {
    /**
     * The constant MINIMUM_PLAYERS.
     */
    public static final int MINIMUM_PLAYERS = 2;
    /**
     * The constant MAXIMUM_PLAYERS.
     */
    public static final int MAXIMUM_PLAYERS = 6;
    /**
     * The constant FIRST_DRAW_CARDS.
     */
    public static final int FIRST_DRAW_CARDS = 5;
    /**
     * The constant LOGGER_PATH.
     */
    public static final String LOGGER_PATH = "E:\\Logger\\Log.log";
}
