package utils;

/**
 * The enum Card action.
 */
public enum CardAction {
    /**
     * Invalid action card action.
     */
    INVALID_ACTION,
    /**
     * No action card action.
     */
    NO_ACTION,
    /**
     * Get two cards card action.
     */
    GET_TWO_CARDS,
    /**
     * Get three cards card action.
     */
    GET_THREE_CARDS,
    /**
     * Get five cards card action.
     */
    GET_FIVE_CARDS,
    /**
     * Get ten cards card action.
     */
    GET_TEN_CARDS,
    /**
     * Change type card action.
     */
    CHANGE_TYPE,
    /**
     * Stop card action.
     */
    STOP
}
