package utils;

import java.io.File;
import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * The type My logger.
 */
public class MyLogger {
    private Logger logger;
    private FileHandler file;
    private SimpleFormatter formatter;
    private File directory;

    static private MyLogger singleInstance = null;

    private MyLogger() throws IOException {
        logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
        directory = new File(GameSettings.LOGGER_PATH);
        file = new FileHandler(directory.getAbsolutePath());
        formatter = new SimpleFormatter();
        file.setFormatter(formatter);
        logger.addHandler(file);
    }

    /**
     * Gets instance.
     *
     * @return the instance
     */
    static public MyLogger getInstance() {
        try {
            if (singleInstance == null) {
                singleInstance = new MyLogger();
            }
        } catch (IOException e) {
            e.getMessage();
        }
        return singleInstance;
    }

    /**
     * Log.
     *
     * @param level   the level
     * @param message the message
     */
    public void log(Level level , String message){
        logger.log(level , message);
    }
}