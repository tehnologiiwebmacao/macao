package utils;

/**
 * The enum Game status.
 */
public enum GameStatus {
    /**
     * Not initialized game status.
     */
    NOT_INITIALIZED,
    /**
     * Initialized game status.
     */
    INITIALIZED,
    /**
     * Started game status.
     */
    STARTED,
    /**
     * End game status.
     */
    END,
}
