package model;

import java.io.Serializable;

/**
 * Model class for User Table from DB.
 */
public class User implements Serializable {
    private Integer idUser;
    private String username;
    private String password;

    /**
     * Gets id user.
     *
     * @return the id user
     */
    public Integer getIdUser() {
        return idUser;
    }

    /**
     * Sets id user.
     *
     * @param idUser the id user
     */
    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    /**
     * Gets username.
     *
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets username.
     *
     * @param username the username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Gets password.
     *
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets password.
     *
     * @param password the password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Instantiates a new User.
     *
     * @param username the username
     * @param password the password
     */
    public User(String username, String password) {
        super();
        this.username = username;
        this.password = password;
    }

    /**
     * Instantiates a new User.
     *
     * @param idUser   the id user
     * @param username the username
     * @param password the password
     */
    public User(Integer idUser, String username, String password) {
        super();
        this.idUser = idUser;
        this.username = username;
        this.password = password;
    }

    @Override
    public String toString() {
        return "Users [iduser= " + idUser + ", password= " + password + ", username= " + username + "]";
    }
}